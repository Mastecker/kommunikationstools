# Kommunikationstools

* [Kurs als Ebook](https://mastecker.gitlab.io/kommunikationstools/course.epub)
* [Kurs als PDF](https://mastecker.gitlab.io/kommunikationstools/course.pdf)
* [Kurs als HTML](https://mastecker.gitlab.io/kommunikationstools/index.html)
