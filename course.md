# Kommunikationstools
## ... zum besseren Austausch in der Lehre

Insbesondere beim Lehren und Lernen mit digitalen Medien sind Kommunikationstools nicht mehr wegzudenken. 
Die Kommunikation im virtuellen Raum kann zunächst unterschieden werden in unidirektionalen und bidirektionalen Austausch. Für eine Ankündigung oder Terminbekanntgabe eignet sich eine unidirektionale (einseitige) Kommunikation. Dahingegen müssen Abstimmungen oder Diskussionen in einem bidirektionalen (wechselseitigen) Austausch erfolgen.
Bei der Entscheidungsfindung für ein bestimmtes Kommunikationswerkzeug ist weiterhin die Unterscheidung in synchrone und asynchrone Kommunikationsformen entscheidend. Der Einsatzzweck sowie der technische und zeitliche Aufwand unterscheiden sich dahingehend deutlich. Bei der synchronen Kommunikation erfolgt der Austausch annähernd zur gleichen Zeit, bei der asynchronen Kommunikation hingegen zeitversetzt.

### Synchrone und asynchrone Methoden
Im E-Learning Bereich können synchrone Methoden ein sehr hilfreiches Tool sein, weil sie reale Bedingungen eines Gesprächs schaffen können und sich besonders für die Kommunikation in größeren Gruppen eignen, bei der nicht alle vor Ort sein können. Andererseits ist ein gewisses technisches Geschick, sowohl von den Lehrenden als auch von den Lernenden, erforderlich und die technischen Kommunikationswerkzeuge sind durch eine fehlende oder eingeschränkte Internetverbindung störanfällig.
Bei einer zeitlichen Verzögerung der Interaktion kommen asynchrone Kommunikationswerkzeuge zum Einsatz. Dabei ist jeder Teilnehmer selbst dafür verantwortlich ob und was gelernt wird. Im Bereich des E-Learnings gibt es weitaus mehr asynchrone als synchrone Kommunikationsmethoden. Auch das Bereitstellen von Dokumenten zum Download stellt eine asynchrone Methode dar.

### Synchrone Kommunikationstools
Als synchrones Kommunikationstools kann der Chat und das virtuelle Klassenzimmer festgehalten werden. Im Folgenden werden diese beiden Tools erläutert und in ihrem Einsatz erklärt.

##### Was ist ein Chat?
Ein Chat ermöglicht es den Teilnehmern auf schriftliche Art gleichzeitig miteinander zu kommunizieren. Da ein Chat oftmals im privaten Bereich eingesetzt wird, hat der Einsatz eines Chats im Unterricht oder an der Universität erst selten Einsatz in die Praxis gefunden. 
Bei einem Chat wird eine sog. Chatiquette aufgestellt, die die „Benimm“-Regeln für die Kommunikation und den gemeinsamen Umgang miteinander enthält. Einige Varianten möglicher Chatiguetten finden Sie unter http://www.chatiquette.de. Zu Beginn sollte kurz vom Moderator oder Leiter des Chats angekündigt werden wie lange der Chat in etwa dauert, wie das Thema lautet, wer moderiert und wie der Ablauf ist.
Nach der Arbeit in einem Chat kann ein Nachprotokoll automatisch erstellt werden, das jedem Teilnehmer zur Verfügung gestellt werden kann und wie eine Mitschrift des Seminarinhaltes sein kann.
Ein gut geplanter und thematisch klar definierter Chat kann ein sehr effizientes Werkzeug für die Online-Kommunikation in Seminaren sein.
In Learning Management Systemen (LMS) sind oftmals Chatfunktionen integriert. Weiterhin gibt es aber auch einige kostenlose Programme (z.B. WhatsApp, Telegram, RocketChat, Slack u.v.m.).

##### Was ist ein virtuelles Klassenzimmer?
Ein virtuelles Klassenzimmer enthält mehrere Kommunikationswerkzeuge auf einer Benutzeroberfläche und kann für synchrone E-Learning-Szenarien eingesetzt werden. Dazu gehören ein Chat, ein Sprachkanal, ein Whiteboard oder eine Präsentationsfläche. Es gelten dieselben Regeln, wie bei einem normalen Chat. Zusätzlich kann über ein Mikrofon miteinander gesprochen und auf dem Whiteboard kann gezeichnet und geschrieben werden. Zusätzlich gibt es Plattformen, bei denen auch eine Präsentation ablaufen kann.
So kann der Chat genutzt werden, um die Notizen auf dem Whiteboard zu kommentieren oder das Whiteboard wird genutzt, um das Geschriebene im Chat zu verdeutlichen. Auf dem Whiteboard kann nicht nur geschrieben, sondern auch Grafiken erstellt, MindMaps entwickelt, Abfragen durchgeführt, Präsentationen angezeigt und Websites gemeinsam betrachtet werden.
Als Vorbereitung für die Arbeit mit dem virtuellen Klassenzimmer sollte den Lernenden die Methode kurz vorgestellt und die Möglichkeit gegeben werden, das Tool in einer ersten Testphase kennenzulernen.
Ähnlich wie bei einem normalen Chat, kann auch hier automatisch ein Protokoll zur Verfügung gestellt werden, das der Nachbereitung dient (evtl. durch eine Videoaufzeichnung).
Der große Vorteil dieses Werkzeuges ist es, dass in Echtzeit mit den Teilnehmern kommuniziert werden kann und zusätzlich Dokumente und Präsentationen gemeinsam betrachtet werden können. Zudem kann gemeinsam an einer MindMap oder Grafik gearbeitet werden.
Es gibt einige kommerzielle Plattformen, die über ein virtuelles Klassenzimmer verfügen. Dazu zählen Citrix, Adobe Connect, Webex, Spreed, OpenMeetings, Zoom, etc. 
Image Video zu Adobe Connect im E-Learning-Bereich: 				
https://www.youtube.com/watch?v=IE_FK298Icg

### Asynchrone Kommunikationstools
Als asynchrone Kommunikationstools gelten all diejenigen, bei denen die Beteiligten zeitversetzt teilnehmen. Ein Diskussionsforum ist wie ein schwarzes Brett, nur online. E-Mailverkehr gehört ebenso dazu, wird aber in diesem Beispiel nicht erläutert, weil dies vorausgesetzt wird. Zudem das Wiki-Web und ein Weblog.

##### Was ist ein Diskussionsforum?
In einem Diskussionsforum kann jeder Teilnehmer seine Nachrichten und/oder Fragen in bestimmte Kategorien einordnen und für alle sichtbar schalten. So können Fragen gestellt, Antworten gegeben und Diskussionen geleitet werden. Dies ist auch im Nachhinein verfügbar und stellt eine Übersicht über alle Beiträge zu einem bestimmten Thema bereit. Die TeilnehmerInnen können unabhängig von Ort und Zeit miteinander kommunizieren, es können Lernmaterialien zur Verfügung gestellt werden und alle haben Zugriff auf die gestellten Beiträge.
Wenn Sie mit einem Diskussionsforum im Rahmen eines Seminars oder einer Klasse arbeiten möchten, sollten Sie zuvor ein benutzerfreundliches Diskussionsforen-Tools auswählen (z.B. in Stud.IP). Die Teilnehmer sollten dann per Nachricht oder E-Mail informiert und eingeladen werden, der Sinn und Zweck sollte in der Nachricht offenbart werden. Der Lehrende sollte die Teilnehmer zur Diskussion und Nutzung des Forums anregen und die ersten Themen selbst bereitstellen. Ratsam ist es, darauf hinzuweisen, dass das Diskussionsforum mindestens zwei Mal pro Woche genutzt und auch eine festgelegte Antwortzeit nicht überschritten werden sollte. Für jedes Seminar benötigen Sie folgende Foren:

**I. Cafeteria** zum informellen Gedankenaustausch
**II. Organisatorisches** für offizielle Mitteilungen, Termine, Prüfungen u.v.m.
**III. Diskussionsforen** zu Fachthemen: Für jedes Thema sollte ein neues Diskussionsforum eröffnet werden, damit Sie und auch Ihre Teilnehmer einen guten Überblick haben.
**IV. FAQ’s** sind sehr sinnvoll und vermeiden im Vorfeld, dass Sie immer wieder die gleichen Fragen beantworten müssen.
**V. Dokumentencontainer:** Learning-Management-Systeme (LMS) sind meist mit einem Dokumentencontainer ausgestattet, die allen 
Seminarteilnehmern zur Verfügung stehen. Ebenso ist es möglich, an Beiträge Dokumente anzuhängen.

Diskussionsforen können insbesondere zur Begleitung von Präsenzseminaren eingesetzt werden, um die Teilnehmer über die Ergebnisse von Arbeitsgruppen zu informieren, schriftliche Materialien und Seminararbeiten zur Verfügung zu stellen, eine Diskussion, die im Seminar begonnen wurde, weiterzuführen, von Lernerfahrungen der anderen zu profitieren. 

##### Was ist ein Wiki-Web?
Kurz gefasst ist ein Wiki-Web ein Online-Werkzeug, um gemeinsam Inhalte zu erstellen oder zu bearbeiten. Wiki ist der hawaiianische Ausdruck für „schnell“. Jeder, der eine Wiki-Seite besucht, kann die Inhalte bearbeiten.
Das bekannteste Beispiel ist das im Jahr 2001 gegründete Wikipedia-Projekt (https://www.wikipedia.org). Alle Personen, die Bearbeitungen auf dieser Seite vornehmen, arbeiten freiwillig an dieser riesigen Enzyklopädie. Thematisch werden nahezu alle Wissensbereiche abgedeckt.
Welche Bearbeitungen vorgenommen werden sind in solchen Wiki-Webs immer einsehbar und öffentlich. Das bedeutet, wenn jemand Informationen löscht, hinzufügt oder überschreibt, können die vorherigen Versionen dennoch angeschaut und ggf. reaktiviert werden. 
Die meisten Learning Management Systeme (LMS, wie z.B. Stud.IP) verfügen über ein Wiki. Ansonsten kann auf einfachem und kostenlosem Wege in Wiki-Web initiiert werden (http://wiki.qualifizierung.com).
Bei der Arbeit mit einem Wiki ist es besonders ratsam, die Teilnehmer über ihre genauen Aufgaben zu informieren und festzulegen wann welche Inhalte erarbeiten sein sollen. 

##### Was ist ein Weblog?
Log ist die Kurzform von Logbook und beinhaltet das Prinzip eines Tagebuchs/Logbuchs. Weblogs sind meist private Internetseiten, die kurze Artikel und Beiträge oder Links zu einem bestimmten Thema oder privaten Erlebnissen teilen. Bekanntester Vertreter ist Wordpress. Es gibt auch eine „Blog-Farm“ an der Universität Göttingen, die vom Stud.IT betrieben wird und für jedes Mitglied der Universität zur Verfügung steht (http://blog.stud.uni-goettingen.de/). Die Einträge werden chronologisch strukturiert, sodass die neuesten Einträge für die Besucher direkt sichtbar sind.
Weblogs dienen neben etablierten Online-Zeitungen als zusätzliche Informationsquelle und stellen subjektiv geführte und schnelle Berichterstattungen vor.
Es gibt nützliche Zusatzfunktionen, wie z.B. die Suchfunktion, die das Durchsuchen aller Beiträge nach bestimmten Schlagwörtern, erlaubt. Zudem können einzelne Beiträge kommentiert und bewertet werden, was den Nutzern erlaubt in einen Diskurs zu treten. Durch die Einordnung in Kategorien kann ein Weblog gut strukturiert werden. Die einzelnen Beiträge können mit Dokumenten und Bildern unterstützt und kategorisiert werden, um bei-spielsweise Bilder in einem Fotoblog zur Verfügung zu stellen. 
Der Einsatz eines Web-Logs in der Lehre zeichnet sich besonders durch folgende Funktionen aus:
-	die eigene professionelle Tätigkeit der Lehrenden kann präsentiert werden
-	Begleitung einer Lehrveranstaltung
-	Eine Reflexion der eigenen Tätigkeit in Form eines Lerntagebuchs, E-Portfolio
-	Als Begleitung eines wissenschaftlichen Projekts (z.B. Evaluationsstudie, wissenschaftliche Arbeit, etc.)
-	Werkzeug für das Wissensmanagement

